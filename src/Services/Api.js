import axios from "axios";


        const API_KEY = "283cea5e23b4a24dbdb3c3a4344df3a8";
        const url = "https://api.openweathermap.org/data/2.5";
    
        
        export const WeatherData = (data) => {
            const {
                coord: { lat, lon },
                main: { temp, feels_like, temp_min, temp_max, humidity },
                name,
                dt,
                sys: { country, sunrise, sunset },
                wind: { speed },
            } = data;

            return {
                lat,
                lon,
                temp,
                feels_like,
                temp_min,
                temp_max,
                humidity,
                name,
                dt,
                country,
                sunrise,
                sunset,
                speed,
                description: data.weather[0].description,
                icon: data.weather[0].icon,
            };
        };
        
       
          
    
    export const fetchData = async (formattedCity) => {
        const { data: { city: { name }, list } } = await axios.get(
          `${url}/forecast?q=${formattedCity}&appid=${API_KEY}&units=metric`
        );
      
        const futureDays = list.filter((_, i) => i % 8 === 0).map((day) => ({
          date: new Date(day.dt_txt),
          max_temp: day.main.temp_max,
          min_temp: day.main.temp_min,
          weather: {
            main: day.weather[0].main,
            description: day.weather[0].description,
            icon: day.weather[0].icon,
          },
        }));
      
        return { formattedCity: name, futureDays };
      }
          
            

    // eslint-disable-next-line import/no-anonymous-default-export
    
    