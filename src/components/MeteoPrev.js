import React from 'react';
import { Box, Heading, Stack, Image, Grid, Text, Tabs, TabList, TabPanels , Tab , TabPanel} from "@chakra-ui/react"

function MeteoPrev({ FutureWeather }) {
  


  return (
    <Box>
        <Box>
            <Tabs variant='unstyled'>
                <TabList>
                    {FutureWeather && FutureWeather.futureDays && FutureWeather.futureDays.map((i: any, index: number) => (
                            <Tab _selected={{border : '2px solid blue', borderRadius :'10px' , py :'20px', bg : 'red.50'}} w='100%' h='100%' display={'flex'} flexDir='column'>
                                <Heading textAlign={'center'} fontFamily={'monospace'} size='sm'>{new Date(i.date).toLocaleDateString()}</Heading>
                                <Box p='3' m='1'>
                                    <Stack direction={'row'}>
                                    
                                        <Heading textAlign={'center'} fontFamily={'monospace'} size='sm'>{i.max_temp.toFixed(0)}° </Heading>
                                        <Heading textAlign={'center'} color='gray.400' fontFamily={'monospace'} size='sm'>{i.min_temp.toFixed(0)}°</Heading>
                                    </Stack>
                                    <Image display={'block'} ml='auto' mr='auto' src={`http://openweathermap.org/img/wn/${i.weather.icon}.png`} w='50%' />
                                </Box>
                                <Text textAlign={'center'}>{i.weather.description}</Text>
                            </Tab>
                        ))
                    }
                    


                </TabList>
                <TabPanels>
                    {
                       FutureWeather && FutureWeather.futureDays && FutureWeather.futureDays.map((i : any) => <TabPanel>
                                                
                        </TabPanel>)
                    }
                </TabPanels>
            </Tabs>
            <Grid templateColumns='repeat(7, 1fr)' gap='2'>

            </Grid>
        </Box>
    </Box>
)
}
   export default MeteoPrev;