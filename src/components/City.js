import {  Button } from "@chakra-ui/react";
import { Input} from '@chakra-ui/react';
import { useToast} from "@chakra-ui/react";
import {nanoid} from 'nanoid';
import { useState } from "react";
import { fetchData } from '../Services/Api';
import MeteoPrev from "./MeteoPrev";


function City({addCity}) {
  const toast = useToast();
  const [content, setContent] = useState('');
  const [ futureDays, setfutureDays] = useState([]);
 
  function HandleSubmit(e) {
    e.preventDefault();
    if (!content) {
      toast({
        title: ' Enter City',
        status: 'error',
        duration: 2000,
        isClosable: true,
      });
      return;
    }
    let formattedCity = content;
  
    const city = {
      id: nanoid(),
      body: content,
    };
  
    addCity(city);
    setContent('');
    
    fetchData(formattedCity).then((res) => setfutureDays(res));
    
  
    } console.log( "futureDays",futureDays);
  
  
  return (
    
      <form onSubmit={HandleSubmit}>
   <Input placeholder='Nom de ville ou Code postal....'
    value={content}    
    onChange= {(e) => setContent (e.target.value)}/> <br/>

     <Button mt={4} type="submit"
     fontWeight='extrabold'>
       Afficher la prévision
        </Button>
        <MeteoPrev FutureWeather={futureDays} />
      </form>
       
  );
}
export default City;

