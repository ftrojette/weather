import './App.css';
import { VStack,Heading, Box, StackDivider} from '@chakra-ui/react';
import City from './components/City';
import { useState,useEffect} from "react";


function App() {  
  
  const [cities, setCities] = useState(
    () => JSON.parse(localStorage.getItem('cities')) || []
  );
  
  useEffect(() => {
    localStorage.setItem('cities', JSON.stringify(cities));
  }, [cities]);
  
  function addCity(city) {
    setCities([...cities, city]);
  }
 


return (
   
  <VStack divider={<StackDivider borderColor='gray.200' />}
  spacing={4}
  align='center'
  p={4}>
   <Box> <Heading > WEATHER
   </Heading></Box> 
      <City addCity={addCity} />
    

   </VStack>
     
  );}


export default App;